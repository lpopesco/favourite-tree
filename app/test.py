from favourite_tree import app
import json, unittest

class TestAPIResponse(unittest.TestCase):
    client = app.test_client()

    def test_favourite_tree(self):
        response = self.client.get('/tree')
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual("Willow", data["myFavouriteTree"])