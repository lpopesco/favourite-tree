#! /usr/bin/env python3
from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/tree', methods=['GET'])
def favourite_tree():
    return jsonify({"myFavouriteTree": "Willow"}), 200

if __name__ == '__main__':
    app.run(debug=True)