#! /usr/bin/env bash

set -e

if [[ -z $1 ]]; then
    echo "Please specify the desired image name:tag as the first argument"
    exit 1
fi

printf "==========================\nBuilding and pushing image\n==========================\n"
docker build ./app -t $1
docker push $1

printf "==========================\nStarting Minikube\n==========================\n"
minikube start --addons=ingress

printf "==========================\nWaiting to ensure ingress is up\n==========================\n"
sleep 20

printf "==========================\nDeploying Favourite Tree Application\n==========================\n"
kubectl apply -f deploy/namespace.yaml # Apply namespace first
cat deploy/* | sed "s|__IMAGE_NAME__|$1|g" | kubectl apply -f - # Replace __IMAGE_NAME__ with provided name

echo "Application successfully deployed on https://local.company.net!"
echo "To access, run the following command:"
echo "sudo echo $(minikube ip) local.company.net >> /etc/hosts"