# Favourite Tree

## Application
The webserver application is written in Python using flask. It contains one test, which verifies the JSON content on the /trees endpoint.

### Running locally
To run the webserver locally:

* Create a virtual environment and activate it: `python3 -m virutalenv .env; source .env/bin/activate`
* Install the requirements: `pip install -r requirements.txt`
* Run the application (debug): `flask --app favourite_tree --debug run`
* Run the application (production): `waitress-serve favourite_tree:app`

### Tests
To run the tests, run the following command in the `app` directory:

`python -m unittest discover .`

## Kubernetes manifests

The deployment is handled via kubectl manifests, within the `deploy` directory. The following resources are created:

* The namespace favourite-tree
* A deployment for the favourite-tree application with a single replica
    * In order to support a dynamic image name from the deployment script (see below), the deployment manifest uses a placeholder image name.
* A service resource to expose the deployment
* An ingress resource to route requests to `local.company.net` to the above service

## Deploy script

The deploy script is responsible for building the favourite-tree container image, pushing it to a registry, spinning up minikube, then deploying the favourite-tree application. The script requires a single argument: The image name.

Example usage: `./build-and-deploy.sh registry.gitlab.com/lpopesco/favourite-tree:latest`

The script first builds the image and tags it with whatever argument is passed, then pushes the image. The next stage starts minikube, with the ingress addon enabled. To make sure the ingress addon has time to fully start, the script waits for 20 seconds. Then, the placeholder `__IMAGE_NAME__` is replaced with the provided name in the deployment manifest, after which all manifests are applied to the cluster.

## Bonus

### Multiple configs (staging, production, etc)

Using a proper templating engine would be the best way to support multiple configs. Some examples of these are helm and kustomize. To build upon this even further, a gitops tool like Flux or ArgoCD could be used in conjunction with Kustomize overlays to manage the deployment.

### Autoscale based on network latency

To scale based on network latency instead of CPU, custom metrics can be used. These are metrics that an application can expose to control when it scales. An example way to implement this is using KEDA, which allows applications to scale based on Prometheus/Thanos.